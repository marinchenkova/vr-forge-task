﻿using Model;

namespace View.Base {

    public interface IDetectModeSelectorView {

        void OnDetectModeSelected(DetectMode mode);

    }

}