﻿namespace View.Base {

    public interface IOnTapAnimationPlayerView {

        void RestartAnimation();

    }

}