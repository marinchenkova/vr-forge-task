﻿using Model;

namespace View.Base {

    public interface IDetectModePublisherView {

        void Publish(DetectMode mode);

    }

}