﻿using Model;
using UnityEngine;

namespace View.Data {

    [CreateAssetMenu(menuName = "App/DetectModeConfig", fileName = "DetectModeConfig")]
    public class DetectModeConfig : ScriptableObject {

        [SerializeField]
        private DetectMode _initialDetectMode;

        public DetectMode InitialDetectMode => _initialDetectMode;
        
    }

}