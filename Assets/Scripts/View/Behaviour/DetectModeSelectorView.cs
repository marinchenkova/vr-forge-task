﻿using Controller;
using Model;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using View.Base;
using View.Data;

namespace View.Behaviour {

    public class DetectModeSelectorView : MonoBehaviour, IDetectModeSelectorView {

        [SerializeField] 
        private DetectModeConfig _detectModeConfig;
    
        [SerializeField]
        private DetectModePublisherView _publisherView;
    
        [SerializeField]
        private ARTrackedImageManager _trackedImageManager;

        [SerializeField]
        private ARPlaneManager _planeManager;

        private DetectModeSelectorController _controller;

        private void Awake() {
            DisableDetection();
            _controller = new DetectModeSelectorController(this);
        }

        private void OnEnable() {
            _publisherView.DetectModePublisher.Subscribe(_controller);
        
        }

        private void OnDisable() {
            _publisherView.DetectModePublisher.Unsubscribe(_controller);
            DisableDetection();
        }

        private void Start() {
            _publisherView.Publish(_detectModeConfig.InitialDetectMode);
        }

        public void OnDetectModeSelected(DetectMode mode) {
            switch (mode) {
                case DetectMode.Plane:
                    EnablePlaneDetection();
                    break;
            
                case DetectMode.Marker:
                    EnableMarkerDetection();
                    break;
            }
        }

        private void DisableDetection() {
            _planeManager.enabled = false;
            _trackedImageManager.enabled = false;
        }

        private void EnablePlaneDetection() {
            _trackedImageManager.enabled = false;
            _planeManager.enabled = true;
        }
    
        private void EnableMarkerDetection() {
            _planeManager.enabled = false;
            _trackedImageManager.enabled = true;
        }
    
    }

    
}
