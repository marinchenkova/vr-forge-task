﻿using Controller;
using Model;
using UnityEngine;
using UnityEngine.UI;
using View.Base;

namespace View.Behaviour {

    public class DetectModePublisherView : MonoBehaviour, IDetectModePublisherView {
        
        [SerializeField]
        private Button _modePlaneButton;
        
        [SerializeField]
        private Button _modeMarkerButton;

        private readonly DetectModePublisherController _controller = new DetectModePublisherController();

        public IPublisher<IDetectModeChanged> DetectModePublisher => _controller;

        private void OnEnable() {
            _modePlaneButton.onClick.AddListener(ModePlaneSelected);
            _modeMarkerButton.onClick.AddListener(ModeMarkerSelected);
        }

        private void OnDisable() {
            _modePlaneButton.onClick.RemoveListener(ModePlaneSelected);
            _modeMarkerButton.onClick.RemoveListener(ModeMarkerSelected);
        }

        public void Publish(DetectMode mode) {
            switch (mode) {
                case DetectMode.Plane:
                    ModePlaneSelected();
                    break;
                
                case DetectMode.Marker:
                    ModeMarkerSelected();
                    break;
            }
        }

        private void ModePlaneSelected() {
            _modePlaneButton.interactable = false;
            _modeMarkerButton.interactable = true;
            _modePlaneButton.Select();

            _controller.OnModePlaneSelected();
        }

        private void ModeMarkerSelected() {
            _modeMarkerButton.interactable = false;
            _modePlaneButton.interactable = true;
            _modeMarkerButton.Select();

            _controller.OnModeMarkerSelected();
        }

    }

}