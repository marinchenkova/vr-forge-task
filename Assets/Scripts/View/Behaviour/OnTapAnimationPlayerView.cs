﻿using Controller;
using UnityEngine;
using View.Base;
using View.Inputs;

namespace View.Behaviour {

    public class OnTapAnimationPlayerView : MonoBehaviour, IOnTapAnimationPlayerView {

        [SerializeField]
        private InputChannel _inputChannel;

        [SerializeField]
        private Animation _animation;

        private OnTapAnimationPlayerController _controller;

        private void Awake() {
            _controller = new OnTapAnimationPlayerController(this);
        }

        private void OnEnable() {
            _inputChannel.InputPublisher.Subscribe(_controller);
        }

        private void OnDisable() {
            _inputChannel.InputPublisher.Unsubscribe(_controller);
        }

        public void RestartAnimation() {
            if (_animation.isPlaying) _animation.Stop();
            _animation.Play();
        }
        
    }

}