﻿using Controller;
using Model;
using UnityEngine;

namespace View.Inputs {

    [CreateAssetMenu(menuName = "App/InputChannel", fileName = "InputChannel")]
    public class InputChannel : ScriptableObject {

        private readonly InputChannelController _controller = new InputChannelController();

        public IPublisher<IInputListener> InputPublisher => _controller;
        public IInputListener InputListener => _controller;

    }

}