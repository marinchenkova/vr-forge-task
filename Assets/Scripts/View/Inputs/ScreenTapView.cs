﻿using Controller;
using UnityEngine;
using UnityEngine.EventSystems;

namespace View.Inputs {

    public class ScreenTapView : MonoBehaviour {

        [SerializeField]
        private InputChannel _inputChannel;

        private InputController _controller;

        private void Awake() {
            _controller = new InputController(_inputChannel.InputListener);
        }

        private void Update() {
            foreach (var touch in Input.touches) {
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId) && touch.phase == TouchPhase.Began) {
                    _controller.OnTap();    
                }
            }
        }

    }

}