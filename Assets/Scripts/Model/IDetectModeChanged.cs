﻿namespace Model {

    public interface IDetectModeChanged {

        void OnDetectModeChanged(DetectMode mode);

    }

}