﻿namespace Model {

    public interface IPublisher<in T> {

        void Subscribe(T listener);
        
        void Unsubscribe(T listener);

    }

}