﻿using Model;
using View.Base;

namespace Controller {

    public class OnTapAnimationPlayerController : IInputListener {
        
        private readonly IOnTapAnimationPlayerView _view;

        public OnTapAnimationPlayerController(IOnTapAnimationPlayerView view) {
            _view = view;
        }

        public void OnTap() {
            _view.RestartAnimation();
        }

    }

}