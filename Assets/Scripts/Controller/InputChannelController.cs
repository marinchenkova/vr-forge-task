﻿using System.Collections.Generic;
using Model;

namespace Controller {

    public class InputChannelController : IPublisher<IInputListener>, IInputListener {

        private readonly List<IInputListener> _listeners = new List<IInputListener>();
        
        public void Subscribe(IInputListener listener) {
            _listeners.Add(listener);
        }

        public void Unsubscribe(IInputListener listener) {
            _listeners.Remove(listener);
        }
        
        public void OnTap() {
            foreach (var listener in _listeners) {
                listener.OnTap();
            }
        }

    }

}