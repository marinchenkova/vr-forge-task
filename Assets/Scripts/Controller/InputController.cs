﻿using Model;

namespace Controller {

    public class InputController {
        
        private readonly IInputListener _listener;

        public InputController(IInputListener listener) {
            _listener = listener;
        }
        
        public void OnTap() {
            _listener.OnTap();
        }

    }

}