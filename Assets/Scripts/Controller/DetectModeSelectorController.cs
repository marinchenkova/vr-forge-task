﻿using Model;
using View.Base;

namespace Controller {

    public class DetectModeSelectorController : IDetectModeChanged {

        private readonly IDetectModeSelectorView _view;

        public DetectModeSelectorController(IDetectModeSelectorView view) {
            _view = view;
        }
        
        public void OnDetectModeChanged(DetectMode mode) {
            _view.OnDetectModeSelected(mode);
        }
        
    }

}