﻿using System.Collections.Generic;
using Model;

namespace Controller {

    public class DetectModePublisherController : IPublisher<IDetectModeChanged> {

        private readonly List<IDetectModeChanged> _listeners = new List<IDetectModeChanged>();

        public void Subscribe(IDetectModeChanged listener) {
            _listeners.Add(listener);
        }

        public void Unsubscribe(IDetectModeChanged listener) {
            _listeners.Remove(listener);
        }

        public void OnModePlaneSelected() {
            NotifyModeChanged(DetectMode.Plane);
        }

        public void OnModeMarkerSelected() {
            NotifyModeChanged(DetectMode.Marker);
        }

        private void NotifyModeChanged(DetectMode mode) {
            foreach (var listener in _listeners) {
                listener.OnDetectModeChanged(mode);
            }
        }

    }

}